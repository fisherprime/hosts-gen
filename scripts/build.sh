#!/bin/bash

BUILD_DIR="$1"

BASE_DIR="$(
	# cd -P .. || exit
	pwd
)"

ldflags="-X 'main.Version=$2' -X 'main.Build=${*:3:2}'"

_build() {
	build_target="$(basename "$1")"

	printf "  > Building: %s\n" "$build_target"
	go build -o "$BUILD_DIR/$build_target/$build_target" \
		-ldflags="$ldflags" -trimpath "$1"
}

if [[ $# -eq 2 ]]; then
	dir="$BASE_DIR/cmd/$2"
	[[ -d "$dir" ]] && _build "$dir"
else
	for dir in "$BASE_DIR"/cmd/*; do
		_build "$dir" &
	done
	wait
fi
