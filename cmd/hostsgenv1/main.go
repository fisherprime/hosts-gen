package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/fisherprime/hosts-gen/internal/v1/hostsgen"
)

func main() {
	if err := hostsgen.Run(); err != nil {
		log.Fatal(err)
	}
}
