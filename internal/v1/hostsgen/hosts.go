// SPDX-License-Identifier: MIT
package hostsgen

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"sync"
)

type (
	// HostsData represents the sections of a downloaded hosts file.
	HostsData struct {
		m        *sync.RWMutex
		comments *strings.Builder
		domains  *StringSlice
	}
)

var (
	ErrCommentNotAdded = errors.New("comment not added")
)

// newHostsData initializes an instance of `HostsData`.
func newHostsData() *HostsData {
	return &HostsData{
		m:        &sync.RWMutex{},
		comments: &strings.Builder{},
		domains:  &StringSlice{},
	}
}

// GetComments from `HostsData` obtained from hosts list comments.
func (h *HostsData) GetComments(ctx context.Context) (comments string, err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		h.m.RLock()
		defer h.m.RUnlock()

		comments = h.comments.String()
	}

	return
}

// GetPrefixedDomains from `HostsData` prefixed with the redirection IP.
func (h *HostsData) GetPrefixedDomains(ctx context.Context) (domains []string, err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		if domains, err = h.GetDomains(ctx); err != nil {
			return
		}
		for index := range domains {
			domains[index] = fmt.Sprintf("%s %s", RedirectIP, domains[index])
		}
	}

	return
}

// GetDomains from `HostsData` (as a ptr to data).
func (h *HostsData) GetDomains(ctx context.Context) (domains []string, err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		h.m.RLock()
		defer h.m.RUnlock()

		domains = sort(*h.domains)
	}

	return
}

// AddComments to the hosts comments list.
func (h *HostsData) AddComments(ctx context.Context, comment string) (err error) {
	if len(comment) < 1 {
		return
	}

	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		h.m.Lock()
		defer h.m.Unlock()

		var written int
		if written, err = fmt.Fprintf(h.comments, "%s\n\n", comment); err != nil {
			return
		}
		if written < 1 {
			err = ErrCommentNotAdded
		}
	}

	return
}

// AddDomain to `HostsData`.
func (h *HostsData) AddDomain(ctx context.Context, domain string) (err error) {
	select {
	case <-ctx.Done():
		err = ctx.Err()
		return
	default:
		h.m.Lock()
		defer h.m.Unlock()

		h.domains.Add(ctx, strings.ToLower(strings.TrimSpace(domain)))
	}

	return
}
