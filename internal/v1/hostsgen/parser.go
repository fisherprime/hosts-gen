// SPDX-License-Identifier: MIT
package hostsgen

import (
	"context"
	"regexp"
	"strings"
)

const (
	commentPrefix = "#"
)

var (
	ipRegex     = regexp.MustCompile(`^(\d{1,3}\.){3}\d{1,3}`)
	domainRegex = regexp.MustCompile(`^[^#\s]`)
)

func parse(ctx context.Context, source string) (err error) {
	var buffer []byte
	if buffer, err = fetch(ctx, source); err != nil {
		return
	}

	content := strings.Split(string(buffer), "\n")
	for index := range content {
		line := content[index]

		switch {
		case strings.HasPrefix(line, commentPrefix):
			if err = HData.AddComments(ctx, line); err != nil {
				return
			}
		case domainRegex.Match([]byte(line)):
			if err = HData.AddDomain(ctx, string(ipRegex.ReplaceAll([]byte(line), []byte("")))); err != nil {
				return
			}
		default:
			// Do nothing.
		}
	}

	return
}
