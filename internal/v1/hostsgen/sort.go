package hostsgen

// REF: https://qvault.io/golang/merge-sort-golang/

func sort(input []string) []string {
	lenItems := len(input)
	if lenItems < 2 {
		return input
	}

	pivot := lenItems / 2
	left, right := sort(input[:pivot]), sort(input[pivot:])
	return merge(left, right)
}

func merge(left []string, right []string) (final []string) {
	indexF, indexL, indexR := 0, 0, 0

	lenL, lenR := len(left), len(right)
	final = make([]string, lenL+lenR)

	for indexL < lenL && indexR < lenR {
		if left[indexL] < right[indexR] {
			final[indexF] = left[indexL]
			indexF++
			indexL++

			continue
		}

		final[indexF] = right[indexR]
		indexF++
		indexR++
	}
	for ; indexL < lenL; indexL++ {
		final[indexF] = left[indexL]
		indexF++
	}
	for ; indexR < lenR; indexR++ {
		final[indexF] = right[indexR]
		indexF++
	}

	return
}
