// SPDX-License-Identifier: MIT
package hostsgen

import "context"

type (
	// StringSlice represents a `[]string`.
	StringSlice []string
)

// Add value to the `StringSlice`.
func (sl *StringSlice) Add(ctx context.Context, val string) {
	select {
	case <-ctx.Done():
		return
	default:
		if sl.Locate(ctx, val) < 0 {
			*sl = append(*sl, val)
		}
	}
}

// Locate a value in a `StringSlice`.
func (sl *StringSlice) Locate(ctx context.Context, val string) (loc int) {
	loc = -1

	select {
	case <-ctx.Done():
		return
	default:
		for index := range *sl {
			if (*sl)[index] == val {
				return index
			}
		}
	}

	return
}
