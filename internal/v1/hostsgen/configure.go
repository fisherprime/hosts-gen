package hostsgen

import (
	"net"
	"net/http"
	"time"
)

const (
	MaxHostConn    = 10
	RequestTimeout = 10 * time.Second
	ParseTimeout   = 15 * time.Second
	RedirectIP     = "0.0.0.0"
)

var (
	GenTransport *http.Transport
	HostSources  []string
	HData        *HostsData
)

func configure() {
	GenTransport = &http.Transport{
		Dial:                (&net.Dialer{Timeout: RequestTimeout}).Dial,
		MaxIdleConnsPerHost: 10,
	}

	HData = newHostsData()
}
