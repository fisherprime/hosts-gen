package hostsgen

import (
	"context"
	"strings"
	"sync"

	log "github.com/sirupsen/logrus"
)

// Run executes the hostsgen main operation.
func Run() (err error) {
	configure()

	ctx, cancel := context.WithTimeout(context.Background(), ParseTimeout)
	defer cancel()

	var wg sync.WaitGroup
	wg.Add(len(HostSources))

	for index := range HostSources {
		index := index
		go func() {
			if err = parse(ctx, HostSources[index]); err != nil {
				log.Error(err)
			}
			wg.Done()
		}()
	}
	wg.Wait()

	var comments string
	if comments, err = HData.GetComments(ctx); err != nil {
		return
	}
	var domains []string
	if domains, err = HData.GetPrefixedDomains(ctx); err != nil {
		return
	}

	var output strings.Builder
	if _, err = output.WriteString(comments); err != nil {
		return
	}
	for index := range domains {
		if _, err = output.WriteString(domains[index]); err != nil {
			return
		}
	}

	return
}
