// SPDX-License-Identifier: MIT
package hostsgen

import (
	"context"
	"errors"
	"io"
	"net/http"
	"net/url"
)

var (
	ErrEmptyResponse = errors.New("empty response")
)

func fetch(ctx context.Context, source string) (buffer []byte, err error) {
	if source == "" {
		// Unexpected situation.
		return
	}

	var src *url.URL
	if src, err = url.Parse(source); err != nil {
		return
	}

	var req *http.Request
	if req, err = http.NewRequestWithContext(ctx, http.MethodPost, src.String(), nil); err != nil {
		return
	}

	client := &http.Client{
		Timeout:   RequestTimeout,
		Transport: GenTransport,
	}

	var resp *http.Response
	resp, err = client.Do(req)
	if resp != nil {
		defer func() { _ = resp.Body.Close() }()
	} else {
		err = ErrEmptyResponse
		return
	}
	if err != nil {
		return
	}

	return io.ReadAll(resp.Body)
}
